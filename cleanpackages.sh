#!/bin/bash

# List projects using their path with namespace
projects="\
  sat-mtl/tools/forks/addict \
  sat-mtl/tools/forks/caffe \
  sat-mtl/tools/forks/blender \
  sat-mtl/tools/forks/filterpy \
  sat-mtl/tools/forks/getch \
  sat-mtl/tools/forks/libmapper \
  sat-mtl/tools/forks/librealsense \
  sat-mtl/tools/forks/mmcv \
  sat-mtl/tools/forks/mmdetection \
  sat-mtl/tools/forks/mmpose \
  sat-mtl/tools/forks/opencv \
  sat-mtl/tools/forks/openpose \
  sat-mtl/tools/forks/pycocotools \
  sat-mtl/tools/forks/python-websockets \
  sat-mtl/tools/forks/pytorch \
  sat-mtl/tools/forks/pytorch-third-party-backports \
  sat-mtl/tools/forks/pytorch-vision \
  sat-mtl/tools/forks/rekall \
  sat-mtl/tools/forks/reprepro \
  sat-mtl/tools/forks/sc3-plugins \
  sat-mtl/tools/forks/supercollider \
  sat-mtl/tools/forks/torch2trt \
  sat-mtl/tools/forks/trt_pose \
  sat-mtl/tools/forks/webmapper \
  sat-mtl/tools/forks/xtcocotools \
  sat-mtl/tools/livepose \
  sat-mtl/tools/ndi2shmdata \
  sat-mtl/tools/shmdata \
  sat-mtl/tools/splash/splash \
  sat-mtl/tools/switcher \
  "

if [[ -z "$token" ]]; then
    echo "Export your private gitlab token before running this script: "
    echo "export token=\"\""
    exit
fi

freed_size=0

# Parse projects
for p in $projects; do
  p=`echo $p | sed 's/\//%2F/g'`

  project=`curl -s https://gitlab.com/api/v4/projects/$p/`;
  name=`echo $project | jq .name`
  echo -e "\e[0;32m List linked packages for project $name...\e[0m"

  # Parse project releases
  releases=`curl -s https://gitlab.com/api/v4/projects/$p/releases`;
  rs=`echo $releases | jq length`;
  echo -e "\e[0;32m Found $rs release(s)"

  # Store urls linked in each release
  r=0;
  url="";
  found=0;
  linked_urls=()
  while [ $r -lt $((rs)) ]
  do
    links=`echo $releases | jq .[$r].assets.links`;
    lss=`echo $links | jq length`;
    l=0;
    while [ $l -lt $((lss)) ]
    do
      filename=`echo $links| jq -r .[$l].name`
      url=`echo $links| jq -r .[$l].url`
      echo "Link name $filename url $url";
    #   echo "Link: $l"
      linked_urls+=("$url")
      ((l++))
    done
    # echo "Release: $r"
    ((r++))
  done;
  echo -e "\e[0;32m Found ${#linked_urls[@]} linked package(s)"

  # Parse project packages
  packages=`curl -s https://gitlab.com/api/v4/projects/$p/packages`;
  pas=`echo $packages | jq length`;
  echo -e "\e[0;32m Found $pas package(s)"

  # Keep packages matching urls linked in releases, otherwise delete
  pa=0;
  url="";
  found=0;
  while [ $pa -lt $((pas)) ]
  do
    id=`echo $packages | jq .[$pa].id`;
    files=`curl -s https://gitlab.com/api/v4/projects/$p/packages/$id/package_files`;

    fs=`echo $files | jq length`;
    f=0;
    while [ $f -lt $((fs)) ]
    do
      fid=`echo $files| jq -r .[$f].id`
      filename=`echo $files| jq -r .[$f].file_name`
      size=`echo $files| jq -r .[$f].size`
    #   echo "Package file id $id name $filename size $size";
      l=0;
      found=0;
      while [ $l -lt ${#linked_urls[@]} ]
      do
        lu=${linked_urls[$l]}
        lid=`echo $lu | sed -r 's/.*\/([0-9]+).*/\1/'`
        if [[ $fid == $lid ]]; then
            found=1;
            break;
        fi
        ((l++))
      done;
      if [[ $found == 1 ]]; then
        echo "Keeping Package file id $fid name $filename size $size";
      else
        echo "Deleting Package file id $fid name $filename size $size";
        freed_size=$((freed_size + size))
        curl --request DELETE --header "PRIVATE-TOKEN: $token" "https://gitlab.com/api/v4/projects/$p/packages/$id/package_files/$fid"
        echo "https://gitlab.com/api/v4/projects/$p/packages/$id/package_files/$fid"
      fi
      ((f++))
    done;
    ((pa++))
  done;
done;

echo "Freed ${freed_size} bytes"
